# EDAF75, project report

This is the report for

 + Gloria Mokberi, `gl6645mo-s`
 + Hanna Jonson, `ha7008jo-s`

We solved this project on our own, except for:

 + The Peer-review meeting


## ER-design


The model is in the file [`E/R UML model.png`](Databasteknik.png):



<center>
    <img src="Databasteknik.png" width="20%">
</center>



## Relations

The ER-model above gives the following relations (neither
[Markdown](https://docs.gitlab.com/ee/user/markdown.html)
nor [HTML5](https://en.wikipedia.org/wiki/HTML5) handles
underlining withtout resorting to
[CSS](https://en.wikipedia.org/wiki/Cascading_Style_Sheets),
so we use bold face for primary keys, italicized face for
foreign keys, and bold italicized face for attributes which
are both primary keys and foreign keys):

+ orders(**order_id**, _company_id_, _company_name_, order_date, deadline)
+ companies(**company_id**, company_name, city, adress, email)
+ delivered_pallets(_pallet_id_, _order_id_, deliver_date)
+ pallet_storage(**pallet_id**, _order_id_, _cookie_name_, production_date, quality_check)
+ specific_orders(_**order_id**_, cookie_amount, _**cookie_name**_)
+ cookies(**cookie_name**)
+ recipes(_**ingredient_name**_, amount, _**cookie_name**_)
+ ingredients(**ingredient_name**, quantity, last_delivery_date, last_delivery_amount, unit)



## Scripts to set up database

The scripts used to set up and populate the database are in:

 + [`create-schema.sql`](create-schema.sql) (defines the tables), and
 + [`initial-data.sql`](initial-data.sql) (inserts data).

So, to create and initialize the database, we run:

```shell
sqlite3 db.sqlite < create-schema.sql
sqlite3 db.sqlite < initial-data.sql
```

(or whatever you call your database file).

## How to compile and run the program

To run the program you need to have python installed on your computer and the bottle framework. 

Run api_fixad.py as follows:

```shell
python api_fixad.py
```
