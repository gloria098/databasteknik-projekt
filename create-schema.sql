Drop TABLE IF EXISTS cookies;
CREATE TABLE cookies (
cookie_name TEXT PRIMARY KEY
);


Drop TABLE IF EXISTS ingredients;
CREATE TABLE ingredients (
ingredient_name TEXT PRIMARY KEY,
quantity INTEGER,
last_delivery_date DATE,
last_delivery_amount INTEGER,
unit TEXT
);

DROP TABLE IF EXISTs companies;
CREATE TABLE companies (
  company_id TEXT PRIMARY KEY,
  company_name TEXT,
  city TEXT,
  adress TEXT,
  email TEXT
);

DROP TABLE IF EXISTS delivered_pallets;
CREATE TABLE delivered_pallets(
  pallet_id TEXT DEFAULT (lower(hex(randomblob(16)))),
  order_id TEXT,
  deliver_date TIMESTAMP,

  FOREIGN KEY (order_id) REFERENCES orders(order_id)
  FOREIGN KEY (pallet_id) REFERENCES pallet_storage(pallet_id)
);

DROP TABLE IF EXISTS orders;
CREATE TABLE orders(
  order_id TEXT PRIMARY KEY,
  company_name TEXT,
  company_id TEXT,
  order_date DATE,
  deadline DATE,

  FOREIGN KEY (company_id) REFERENCES companies(company_id)
  FOREIGN KEY (company_name) REFERENCES companies(company_name)
);

DROP TABLE IF EXISTS pallet_storage;
CREATE TABLE pallet_storage(
  pallet_id TEXT PRIMARY KEY DEFAULT (lower(hex(randomblob(16)))),
  order_id TEXT,
  cookie_name TEXT,
  production_date DATE,
  quality_check INT,

  FOREIGN KEY (cookie_name) REFERENCES cookies(cookie_name)
  FOREIGN KEY (order_id) REFERENCES orders(order_id)
);


DROP TABLE IF EXISTS specific_orders;
CREATE TABLE specific_orders(
  order_id TEXT,
  cookie_amount INTEGER,
  cookie_name TEXT,

  FOREIGN KEY (order_id) REFERENCES orders(order_id)
  FOREIGN KEY (cookie_name) REFERENCES cookies(cookie_name)
  PRIMARY KEY (order_id, cookie_name)
);

DROP TABLE IF EXISTS recipes;
CREATE TABLE recipes(
  ingredient_name TEXT,
  amount INTEGER,
  cookie_name TEXT,

  FOREIGN KEY (ingredient_name) REFERENCES ingredients(ingredient_name)
  FOREIGN KEY (cookie_name) REFERENCES cookies(cookie_name)
  PRIMARY KEY (ingredient_name, cookie_name)
);
