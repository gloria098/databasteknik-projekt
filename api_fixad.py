from bottle import get, post, run, response, request
import sqlite3
import json
from datetime import datetime, date

HOST = 'localhost'
PORT = 8888

connection = sqlite3.connect('db.sql')  # sqlite3.connect('database.db') #skapar en databasfil.


def format_response(d):
    return json.dumps(d, indent=4) + "\n"


# ------------------------------------------------------
# metoder api.py servern kan använd - se nedan

@get('/customers')
def get_customers():
    response.content_type = 'application/json'
    cursor = connection.cursor()
    cursor.execute(
        """
        SELECT company_name, city
        FROM companies
        """
    )
    u = [{"name": company_name, "address": city}
         for (company_name, city) in cursor]

    response.status = 200
    connection.commit()
    return format_response({"customers": u})


@get('/ingredients')
def get_ingredient():
    response.content_type = 'application/json'
    cursor = connection.cursor()
    cursor.execute(
        """
        SELECT ingredient_name, quantity, unit
        FROM ingredients
        """
    )

    u = [{"name": ingredient_name, "quantity": quantity, "unit": unit}
         for (ingredient_name, quantity, unit) in cursor]

    response.status = 200
    connection.commit()
    return format_response({"ingredients": u})


@get('/cookies')  # displays all the cookies
def get_cookies():
    response.content_type = 'application/json'
    cursor = connection.cursor()
    cursor.execute(
        """
        SELECT cookie_name
        FROM  cookies
        """
    )
    u = [{"name": cookie_name[0]}
         for (cookie_name) in cursor]

    response.status = 200
    connection.commit()
    return format_response({"cookies": u})


@get('/recipes')
def get_recipes():
    response.content_type = 'application/json'
    cursor = connection.cursor()
    cursor.execute(
        """
        SELECT cookie_name, ingredient_name, amount, unit
        FROM recipes
        JOIN ingredients
        USING (ingredient_name)
        ORDER BY cookie_name, ingredient_name
        """
    )
    u = [{"cookie_name": cookie_name, "ingredient_name": ingredient_name, "amount": amount, "unit": unit}
         for (cookie_name, ingredient_name, amount, unit) in cursor]

    response.status = 200
    connection.commit()
    return format_response({"recipes": u})


@get('/pallets')  # gives a list of all pallets in the storeroom
def get_pallets():
    response.content_type = 'application/json'
    cursor = connection.cursor()
    connection.commit()

    query = """
		    SELECT pallet_id, cookie_name, production_date, company_name, quality_check
		    FROM pallet_storage
		    LEFT JOIN orders
		    USING (order_id)
            WHERE 1=1
		    """

    params = []

    if request.query.after:
        query = query + "AND production_date > ?"
        params.append(request.query.after)
    if request.query.before:
        query = query + "AND production_date < ?"
        params.append(request.query.before)
    if request.query.cookie_name:
        query = query + "AND cookie_name = ?"
        params.append(request.query.cookie_name)
    if request.query.blocked:
        query = query + "AND quality_check = ?"
        params.append(request.query.blocked)

    cursor.execute(query, params)
    u = [{"id": pallet_id, "cookie": cookie_name, "productionDate": production_date, "customer": company_name,
          "blocked": quality_check}
         for (pallet_id, cookie_name, production_date, company_name, quality_check) in cursor]

    response.status = 200
    connection.commit()
    return format_response({"pallets": u})


# creating pallets
@post('/pallets')
def create_pallet():
    response.content_type = 'application/json'

    cookie = request.query.cookie

    if not cookie:
        response.status = 400
        return format_response({'error': 'missing parameter <cookie>'})

    def cookie_name_exists(cookie):
        query = '''
            SELECT cookie_name 
            FROM cookies
            WHERE cookie_name = ?
        '''
        res = [{'cookie_name': cookie_name}
               for cookie_name
               in connection.cursor().execute(query, [cookie])]
        if not res:
            return False

        return True

    if not cookie_name_exists(cookie):
        response.status = 200
        return format_response({'status': 'no such cookie'})

    def get_ingredients_in_stock():
        query = '''
            SELECT ingredient_name, quantity
            FROM ingredients
        '''
        res = [[ingredient_name,
                quantity_in_stock]
               for (ingredient_name, quantity_in_stock)
               in connection.cursor().execute(query)]
        return res

    def get_cookie_ingredients(cookie):
        query = '''
            SELECT ingredient_name, amount
            FROM cookies
            JOIN recipes
            USING (cookie_name)
            WHERE cookie_name = ?
        '''
        res = [[ingredient_name,
                quantity]
               for (ingredient_name, quantity)
               in connection.cursor().execute(query, [cookie])]
        return res

    # skapar en lokal variabel för att minimera databasoperationerna
    ingredients_in_recipe = get_cookie_ingredients(cookie)
    ingredients_in_stock = get_ingredients_in_stock()

    def enough_ingredients():
        for ingredient_in_recipe in ingredients_in_recipe:
            for ingredient_in_stock in ingredients_in_stock:
                if ingredient_in_recipe[0] == ingredient_in_stock[0]:
                    if not (ingredient_in_recipe[1] * 54 <= ingredient_in_stock[1]):
                        return False
        return True

    def create_new_pallet(cookie):
        def add_pallet(cookie):
            query = '''
                INSERT INTO pallet_storage(cookie_name, order_id, production_date, quality_check)
                VALUES (?,?,?,?)
            '''

            cookie_name = cookie
            order_id = 1  # alltid samma order_id
            production_date = str(date.today())
            is_blocked = 0  # alla pallets är inte blockerade by default

            connection.cursor().execute(query, [cookie_name, order_id, production_date, is_blocked])
            connection.commit()

        def get_latest_pallet():
            query = '''
                SELECT pallet_id
                FROM pallet_storage
                WHERE rowid = last_insert_rowid()
            '''
            return connection.cursor().execute(query).fetchone()[0]

        add_pallet(cookie)
        return get_latest_pallet()

    def update_stock():
        new_stock_values = []

        for ingredient_in_recipe in ingredients_in_recipe:
            for ingredient_in_stock in ingredients_in_stock:
                if ingredient_in_recipe[0] == ingredient_in_stock[0]:
                    diff = ingredient_in_stock[1] - 54 * ingredient_in_recipe[1]
                    new_value = [ingredient_in_stock[0], diff]
                    new_stock_values.append(new_value)

        query = '''
            UPDATE ingredients 
            SET quantity = ?
            WHERE ingredient_name = ?
        '''
        c = connection.cursor()

        for value in new_stock_values:
            c.execute(query, [value[1], value[0]])

        connection.commit()


    if not enough_ingredients():
        response.status = 200
        return format_response({'status': 'not enough ingredients'})
    else:
        new_pallet_id = create_new_pallet(cookie)
        update_stock()

        response.status = 200
        return format_response({'status': 'ok', 'id': new_pallet_id})


#   blocking pallets
@post('/block/<cookie_name>/<from_date>/<to_date>')
def block_pallets(cookie_name, from_date, to_date):
    response.content_type = 'application/json'
    cursor = connection.cursor()
    cursor.execute(
        """
        UPDATE pallet_storage
        SET quality_check = 1
        WHERE cookie_name = ?  AND ? <= production_date AND production_date <= ?
        """,
        [cookie_name, from_date, to_date]
    )
    u = {"status": "ok"}

    response.status = 200
    connection.commit()
    return format_response(u)


# unblocks pallets
@post('/unblock/<cookie_name>/<from_date>/<to_date>')
def unblock_pallets(cookie_name, from_date, to_date):
    response.content_type = 'application/json'
    cursor = connection.cursor()
    cursor.execute(
        """
        UPDATE pallet_storage
        SET quality_check = 0
        WHERE cookie_name = ?  AND ? <= production_date AND production_date <= ?
        """,
        [cookie_name, from_date, to_date]
    )
    u = {"status": "ok"}

    response.status = 200
    connection.commit()
    return format_response(u)


@post('/reset')
def reset():
    cursor = connection.cursor()
    fo = open('create-schema.sql')
    cursor.executescript(fo.read())
    fo.close()
    fo = open('initial-data.sql')
    cursor.executescript(fo.read())
    fo.close()
    connection.commit()
    u = {"status": "ok"}

    return format_response(u)


# ------------------------------------------------------------
@get('/ping')
def ping():
    response.status = 200
    connection.commit()
    return "Pong" + response.status + "\n"


run(host=HOST, port=PORT, debug=True)
